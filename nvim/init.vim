"set path+=** " :find fil*name (use tab for completion); :b (f)ilenam (jump to buffered files)

set list
set listchars=tab:>>,trail:.,nbsp:+ "default: tab:>>,trail:-,nbsp:+
set scrolloff=8

set number
set showmatch
set smartcase
set ignorecase

set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set formatoptions-=tc
set formatoptions+=qrojn12

set undolevels=1000

"120th line is marked
highlight ColorColumn ctermbg=4
set colorcolumn=120

"enable automatic folding (annoing because everything is folded at startup)
"set foldmethod=indent

"map mouse-wheel scrolling
set mouse=a
map <ScrollWheelUp> 5<C-Y>
map <ScrollWheelDown> 5<C-E>

"Remove all trailing whitespace by pressing F5
nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

"remap for ESC
inoremap ,m <esc>
nnoremap ,m <esc>
vnoremap ,m <esc>
call plug#begin()
    " Better Visual Guide
    Plug 'Yggdroot/indentLine'
    " syntax checks
    Plug 'w0rp/ale'

    " Autocomplete
    Plug 'ncm2/ncm2'
    Plug 'roxma/nvim-yarp'
    Plug 'ncm2/ncm2-bufword'
    Plug 'ncm2/ncm2-path'
    Plug 'ncm2/ncm2-jedi'

    " Autocomplete C/C++
    Plug 'roxma/ncm-clang'
call plug#end()
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
set shortmess+=c
inoremap <C-c> <esc>
"inoremap <expr> <CR> (pumvisible() ? "\<C-y>\<CR>" : "\<CR>")
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<Tab>"
let ncm2#popup_delay = 5
let ncm2#complete_length = [[1,1]]
let g:ncm2#matcher = 'substrfuzzy'
