"syn keyword customIdentifier self
hi backgroundHL ctermfg=DarkGrey guifg=DarkGrey
hi def link objectHL Special
"hi def link objectHL Typedef
"hi def link objectHL Function
syn match backgroundHL "\<self\ze"
syn match objectHL "\<\u\w\+\ze" "\<\w\+\ze\.
